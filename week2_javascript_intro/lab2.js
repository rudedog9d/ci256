/*
 * 1. Write a function that takes two numeric arguments 
      and returns the square root of their product.
*/
function question1() {}

/*
 * 2. Write a function that returns an array of all prime numbers that have 5 place values.
 */
function question2() {}

/*
 * 3. Write a function that takes two numeric arguments and returns an array of 
      all numbers between them that are divisible by 3.
*/
function question3() {}

/*
 * 4. Write a function that checks if a string is a palindrome, and returns a boolean value.
 */
function question4() {}

/*
 * 5. Write a function that takes an array of numbers and 
      returns another array only containing the odd indexes of the first
   Example:
     input = [0, 4, 5, 8, ‘test’]
     Output: [4, 8]
*/
function question5() {}

/*
 * 6. Write a function that takes a string and returns an array of the vowels (a, e, i, o, u) from that string
 */
function question6() {}

/*
 * 7. Write a function that takes a string and returns an object whose keys
      are the characters in the string and values are the number of occurrences (case sensitive)
   Example:
    input = ‘Tests’
    Output: {
      ‘T’: 1,
      ‘t’: 1,
      ‘e’: 1,
      ‘s’: 2
    }
*/
function question7() {}

/*
 * 8. Write a function that takes two string arguments, 
      removes any characters shared between them, 
      and returns the concatenated result (Case sensitive).
  Example:
    input: “Test”, “Weather”
    Output: “TsWahr”
*/
function question8() {}

/*
 * 9. Write a function that takes two string arguments, removes any characters shared between them, and returns the concatenated result (Case insensitive)
Example: input: “Test”, “Weather”
Output: “sWahr”
*/
function question9() {}

/*
 10. Write a function that takes a array of numbers and returns an object, 
     with each key of the object being “_<number>” and values being that number squared
  Examples:
    input: [1,2,3]
    Output: {
      _1: 1,
      _2: 4,
      _3: 9
    }

    input: [2, 4, 6]
    Output: {
      _2: 4,
      _4: 16,
      _6: 36
    }

  </number>
*/
function question10() {}



// don't touch this!
module.exports = {
  question1,
  question2,
  question3,
  question4,
  question5,
  question6,
  question7,
  question8,
  question9,
  question10,
};
