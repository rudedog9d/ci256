const lab3 = require("./lab2");

describe("question 1", () => {
  describe("finds sqrt of product of", () => {
    test("5 and 7", () => {
      expect(lab3.question1(5 ,7)).toEqual(Math.sqrt(5 * 7));
    });

    test("1532 and 314534", () => {
      expect(lab3.question1(1532, 314534)).toEqual(Math.sqrt(1532 * 314534));
    });
  });
});

describe("question 2", () => {
  test("finds correct number of prime values between 9999 and 100000", () => {
    expect(lab3.question2().length).toEqual(8363);
  });
});

describe("question 3", () => {
  describe("finds values between", () => {
    test("2 and 7", () => {
      expect(lab3.question3(2, 7)).toEqual([3, 6]);
    });

    test("10 and 100", () => {
      expect(lab3.question3(10, 100)).toEqual([
        12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60, 63,
        66, 69, 72, 75, 78, 81, 84, 87, 90, 93, 96, 99,
      ]);
    });
  });
});

describe("question 4", () => {
  test("determines 'racecar' is a palindrome", () => {
    expect(lab3.question4("racecar")).toBe(true);
  });

  test("determines 'level' is a palindrome", () => {
    expect(lab3.question4("level")).toBe(true);
  });

  test("determines 'hello world' is a palindrome", () => {
    expect(lab3.question4("hello world")).toBe(false);
  });

  test("doesn't modify the original string", () => {
    let a = "hello";
    lab3.question4(a);
    expect(a).toBe("hello");
  });
});

describe("question 5", () => {
  describe("returns odd values from", () => {
    test("test array A", () => {
      expect(lab3.question5(['a', 'b', 'c', 'd', 'e'])).toEqual(['b', 'd']);
    });

    test("test array B", () => {
      let arr = [...Array(100).keys()].map(i => i + 1);
      expect(lab3.question5(arr)).toEqual(arr.filter((_, i) => i % 2 === 1));
    });
  });
});

describe("question 6", () => {
  describe("returns vowels from", () => {
    test("'string'", () => {
      expect(lab3.question6('string')).toEqual(['i']);
    });

    test("'hAckeR'", () => {
      expect(lab3.question6('hAckeR')).toEqual(['A', 'e']);
    });
  });
});

describe("question 7", () => {
  describe("returns letters from", () => {
    test("'Tests'", () => {
      expect(lab3.question7('Tests')).toEqual({
        'T': 1,
        't': 1,
        'e': 1,
        's': 2
      });
    });

    test("'hAckeR'", () => {
      expect(lab3.question7('hAckeR')).toEqual({
        'h': 1,
        'A': 1,
        'c': 1,
        'k': 1,
        'e': 1,
        'R': 1
      });
    });
  });
});

describe("question 8", () => {
  describe("returns shared letters (case sensitive) from", () => {
    test("'Test' and 'Weather'", () => {
      expect(lab3.question8('Test', 'Weather')).toEqual('TsWahr');
    });

    test("'no' and 'match'", () => {
      expect(lab3.question8('no', 'match')).toEqual('nomatch');
    });

    test("'asdf' and 'fdsa'", () => {
      expect(lab3.question8('asdf', 'fdsa')).toEqual('');
    });
  });
});

describe("question 9", () => {
  describe("returns shared letters (case insensitive) from", () => {
    test("'Test' and 'Weather'", () => {
      expect(lab3.question9('Test', 'Weather')).toEqual('sWahr');
    });

    test("'no' and 'match'", () => {
      expect(lab3.question9('no', 'match')).toEqual('nomatch');
    });

    test("'asdf' and 'fdsa'", () => {
      expect(lab3.question9('asdf', 'FDAS')).toEqual('');
    });
  });
});

describe("question 10", () => {
  describe("returns object from the array", () => {
    test("[1, 2, 3]", () => {
      expect(lab3.question10([1, 2, 3])).toEqual({
        '_1': 1,
        '_2': 4,
        '_3': 9,
      });
    });

    test("[2, 4, 6]", () => {
      expect(lab3.question10([2, 4, 6])).toEqual({
        '_2': 4,
        '_4': 16,
        '_6': 36,
      });
    });

    test("[3, 6, 9]", () => {
      expect(lab3.question10([3, 6, 9])).toEqual({
        '_3': 9,
        '_6': 36,
        '_9': 81,
      });
    });
  });
});
